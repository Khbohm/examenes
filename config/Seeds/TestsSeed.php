<?php
use Migrations\AbstractSeed;

/**
 * Tests seed.
 */
class TestsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('es_AR');
        $populator = new Faker\ORM\CakePHP\Populator($faker);
        
        $populator->addEntity('Tests', 30, [
            'token' => function() {return uniqid();},
            'user_id' => function() {return rand(1,50);},
            'allowed_attempts' => function() {return rand(1,5);},
            'active' => function(){return rand(0, 1);},
            'created' => function() use ($faker) {return $faker->dateTimeBetween('now', 'now');},
            'modified' => function() use ($faker) {return $faker->dateTimeBetween('now', 'now');},
        ]);
        
        $populator->execute();

    }
}
