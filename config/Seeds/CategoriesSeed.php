<?php
use Migrations\AbstractSeed;

/**
 * Categories seed.
 */
class CategoriesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        
        $data = [
            [
                'name' => 'Categoría general',
                'description' => 'Preguntas generales sobre educación vial.',
                'image' => null,
                'time' => 10,
                'questions_per_test' => 5,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        ];

        $table = $this->table('categories');
        $table->insert($data)->save();        
        
        $data = [
            [
                'text' => 'Se tiene noticia de que en el año 1912 aconteció:',
                'image' => null,
                'category_id' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Nuestros antecesores publicaron de manera oficial el primer código de circulación en el año:',
                'image' => null,
                'category_id' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Debido al aumento desproporcionado de las tasas de accidentes a los inicions de los 70s se promulgaron dos leyes:' ,
                'image' => null,
                'category_id' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Ley que sustituye a la ley 5930 el 22 de abril de 1993:' ,
                'image' => null,
                'category_id' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'La siguiente imágen hace referencia al término:' ,
                'image' => 'triangulo.png',
                'category_id' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Con el nombre de Trilogía Vial se conoce a la siguiente relación:' ,
                'image' => null,
                'category_id' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'El objetivo final es el "normal desplazamiento por la vía pública". El texto anterior hacer referencia a:' ,
                'image' => null,
                'category_id' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
            
        ];        

        $table = $this->table('questions');
        $table->insert($data)->save();        
        
        $data = [
            [
                'text' => 'Se publicó el primer código de circulación.',
                'question_id' => 1,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Circuló el primer vehículo en San José',
                'question_id' => 1,
                'image' => null,
                'correct' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Se instauró la primera gasolinera en el país.',
                'question_id' => 1,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => '1920',
                'question_id' => 2,
                'image' => null,
                'correct' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => '1976',
                'question_id' => 2,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => '1912',
                'question_id' => 2,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => '6324 ley de administración vial y 5930 ley de tránsito.',
                'question_id' => 3,
                'image' => null,
                'correct' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => '5930 ley de administración vial y 6324 ley de tránsito.',
                'question_id' => 3,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => '6324 ley de tránsito por vias públicas terrestres y 5930 ley de administración vial.',
                'question_id' => 3,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Código de Circulación',
                'question_id' => 4,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Ley de Administración Vial, 6324',
                'question_id' => 4,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Ley de Tránsito por vías publicas terrestres, 7331',
                'question_id' => 4,
                'image' => null,
                'correct' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Seguridad Vial',
                'question_id' => 5,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Legislación',
                'question_id' => 5,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Trilogía Vial',
                'question_id' => 5,
                'image' => null,
                'correct' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Ley de tránsito, inspector y carretera.',
                'question_id' => 6,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Factor humano, factor vehícular, factor vial y su entorno.',
                'question_id' => 6,
                'image' => null,
                'correct' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Vehículo, conductor y peatón.',
                'question_id' => 6,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'MOPT',
                'question_id' => 7,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Trilogía vial',
                'question_id' => 7,
                'image' => null,
                'correct' => 1,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'text' => 'Seguridad vial',
                'question_id' => 7,
                'image' => null,
                'correct' => 0,
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        ];  
        
        $table = $this->table('answers');
        $table->insert($data)->save();        
        
        
    }
}
