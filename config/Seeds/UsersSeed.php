<?php
use Migrations\AbstractSeed;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('es_AR');
        $populator = new Faker\ORM\CakePHP\Populator($faker);
        
        $hasher = new DefaultPasswordHasher();
        
        // Crea el administrador
        $populator->addEntity('Users', 1, [
            'email' => 'khbohm@gmail.com',
            'firstname' => 'Claudio Juan',
            'lastname' => 'Böhm',
            'password' => 'Kh123456',
            'role' => 'admin',
            'active' => 1,
            'created' => function() use ($faker) {return $faker->dateTimeBetween('now', 'now');},
            'modified' => function() use ($faker) {return $faker->dateTimeBetween('now', 'now');},
            'reset_token' => null,
            'reseted_at' => null,
            'remember_token' => null,            
        ]);
        
        $populator->execute();
        
        
        // Crea 49 usuarios más
        $populator->addEntity('Users', 49, [
            'email' => function() use ($faker) {return $faker->safeEmail();},
            'firstname' => function() use ($faker) {return $faker->firstName();},
            'lastname' => function() use ($faker) {return $faker->lastName();},
            'password' => 'Ab123456',
            'role' => 'user',
            'active' => function(){return rand(0, 1);},
            'created' => function() use ($faker) {return $faker->dateTimeBetween('now', 'now');},
            'modified' => function() use ($faker) {return $faker->dateTimeBetween('now', 'now');},
            'reset_token' => null,
            'reseted_at' => null,
            'remember_token' => null,
        ]);
        
        $populator->execute();
    }
}
