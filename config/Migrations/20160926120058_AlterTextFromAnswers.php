<?php
use Migrations\AbstractMigration;

class AlterTextFromAnswers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('answers');
        $table->changeColumn('text', 'text');
        $table->update();
    }
    
    public function down()
    {
        $table = $this->table('answers');
        $table->changeColumn('text', 'string', [
            'limit' => 1024,
            'default' => '',
            'null' => false,
        ]);
        $table->update();
    }    
}
