<?php
use Migrations\AbstractMigration;

class RemoveQuestionsPerTestFromCategories extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('categories');
        $table->removeColumn('questions_per_test');
        $table->update();
    }
}
