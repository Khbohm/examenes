<?php
use Migrations\AbstractMigration;

class AddQuantityPerTestToCategoriesTestTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('categories_test_types');
        $table->addColumn('questions_per_test', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => false,
        ]);        
        $table->update();
    }
}
