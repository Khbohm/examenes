<?php
use Migrations\AbstractMigration;

class AddActiveForToTests extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tests');
        $table->addColumn('active_for', 'integer', [
            'default' => 72,
            'limit' => 4,
            'null' => false,
        ]);
        $table->update();
    }
}
