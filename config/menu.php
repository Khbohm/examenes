<?php
return [
	'MenuItems' => [
		'Home' => [
			'icon' => 'home',
			'url' => ['controller' => 'pages', 'action' => 'display', 'home', 'prefix' => 'admin']
		],
		'Tests' => [
			'items' => [
				'New Test'=> [
					'url' => ['controller' => 'tests', 'action' => 'add', 'prefix' => 'admin']
				],
				'Tests list'=> [
					'url' => ['controller' => 'tests', 'action' => 'index', 'prefix' => 'admin']
				],
				
			]
		],
		'Management' => [
			'icon' => 'tachometer',
			'items' => [
				'Users' => [
					'url' => ['prefix' => 'admin', 'controller' => 'Users', 'action' => 'index'],
					'icon' => 'users'
				],
				'separator',
				'Test Types' => [
					'url' => ['prefix' => 'admin', 'controller' => 'TestTypes', 'action' => 'index'],
					'icon' => 'tags'
				],
				'Categories' => [
					'url' => ['prefix' => 'admin', 'controller' => 'Categories', 'action' => 'index'],
					'icon' => 'tags'
				],
				'Questions' => [
					'url' => ['controller' => 'questions', 'action' => 'index', 'prefix' => 'admin']
				]
			]
		]
	]
];
