<!DOCTYPE html>
<html lang="<?= \Locale::getPrimaryLanguage(\Cake\I18n\I18n::locale()) ?>">
<head>
    <?= $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $this->fetch('title') ?></title>
    <meta name="description" content="">
    <meta name="author" content="">

    <?= $this->Html->meta('icon'); ?>
    <?= $this->fetch('meta'); ?>
    <?= $this->fetch('css'); ?>

    <?= $this->Html->css("signin") ?>
    
    <style type="text/css">
    
        body{
            background-color: black;
        }    
    
        .btn {margin-left: auto;}
        
    </style>
    
    <?= $this->fetch('headjs'); ?>
</head>
  <body>

    <div class="container">
        <div class="row"><div class="col-xs-8 col-xs-offset-2 col-md-4 col-md-offset-4"><?= $this->Html->image('logo_f_negro.jpg', ['class' => 'img-responsive center-block']) ?></div></div>
        
        <?= $this->Flash->render(); ?>
        <?= $this->fetch('content'); ?>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

    <?php echo $this->Html->script('ie10-viewport-bug-workaround') ?>
  </body>
</html>