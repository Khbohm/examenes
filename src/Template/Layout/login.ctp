<!DOCTYPE html>
<html lang="<?= \Locale::getPrimaryLanguage(\Cake\I18n\I18n::locale()) ?>">
<head>
    <?= $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $this->fetch('title') ?></title>
    <meta name="description" content="">
    <meta name="author" content="">

    <?= $this->Html->meta('icon'); ?>
    <?= $this->fetch('meta'); ?>
    <?= $this->fetch('css'); ?>

    <?= $this->Html->css("signin") ?>
    
    <style type="text/css">
    
        body{
            //background-image: url(img/fondo.jpg);
            background-color:black;
            background-size: contain;
            background-blend-mode: soft-light;
            color:white;
        }    
    
        .btn {margin-left: auto;}
        
    </style>
    
    <?= $this->fetch('headjs'); ?>
</head>
  <body>

    <div class="container">
        
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8 col-md-offset-5 col-md-2">
                <?= $this->Html->image('logo_f_trans.png', ['class' => 'img-responsive center-block']) ?>
                <br><br>
            </div>
        </div>        
        <h3 class="text-center">Evaluación en línea de conocimientos teóricos.</h3>
        
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8 col-md-offset-3 col-md-6">
                <?= $this->Flash->render(); ?>      
            </div>
        </div>
        
        
        <?= $this->fetch('content'); ?>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

    <?php echo $this->Html->script('ie10-viewport-bug-workaround') ?>
  </body>
</html>