<?php use Cake\I18n\FrozenTime; ?>
<!DOCTYPE html>
<html lang="<?= \Locale::getPrimaryLanguage(\Cake\I18n\I18n::locale()) ?>">
<head>
    <?= $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $this->fetch('title') ?></title>
    <meta name="description" content="">
    <meta name="author" content="">

    <?= $this->Html->meta('icon'); ?>
    <?= $this->fetch('meta'); ?>
    <?= $this->fetch('css'); ?>

    <?= $this->Html->css("signin") ?>
    
    <?= $this->fetch('headjs'); ?>
    <?= $this->Html->script('jquery.timeago'); ?>
    <?= $this->Html->script('locales/jquery.timeago.es'); ?>
</head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header" style="margin-right:30px">
          <a class="navbar-brand" href="#" style="padding:0">
            <?= $this->Html->image('logo_f_trans.png', ['class' => 'img-responsive', 'style' => 'max-height:50px;margin-top:2px;' ]) ?>
          </a>
        <?php $termina = (new FrozenTime($attempt->started_at))->addMinutes($attempt->test->test_type->time); ?>
        <p class="navbar-text pull-right">Termina <time class="timeago" datetime="<?= $termina->i18nFormat('yyyy-MM-dd HH:mm:ss') ?>"><?= $termina->timeAgoInWords(['accuracy' => 'minute'])  ?></time</p>          
          
        </div>
        <p class="navbar-text">
          <div class="progress" style="margin-top:15px;">
            <?php $percent = $number / $total * 100; ?>
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;width:<?= $percent ?>%">
                <?= $this->Number->toPercentage($percent) ?>
            </div>
          </div>
        </p>
      </div>
    </nav>

    <div class="container">
        <?= $this->Flash->render(); ?>
        <?php //pr($total); pr($number); ?>
        <?= $this->fetch('content'); ?>
    </div><!-- /.container -->    


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

    <?php echo $this->Html->script('ie10-viewport-bug-workaround') ?>
    
    <?= $this->fetch('script'); ?>
  </body>
</html>