<div class="collapse navbar-collapse navbar-ex1-collapse">
    <nav>
        <ul class="nav nav-pills">
            <?= $this->cell('CrudView.TablesList', [
                'tables' => \Cake\Utility\Hash::get($actionConfig, 'scaffold.tables'),
                'blacklist' => \Cake\Utility\Hash::get($actionConfig, 'scaffold.tables_blacklist')
            ]) ?>
        </ul>
    </nav>
</div>
