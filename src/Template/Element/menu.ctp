<?php use Cake\Core\Configure; ?>

<ul class="nav navbar-nav">
    <?php
        $menu = Configure::read('MenuItems');

        foreach ($menu as $it => $item) {
            if($item == 'separator') {
                echo '<li role="separator" class="divider"></li>';
            } elseif(array_key_exists('items', $item) ) {
                echo '<li class="dropdown">';
                echo $this->Html->link(__($it).' <span class="caret"></span>', '#', ["class" => "dropdown-toggle", "data-toggle" => "dropdown", 'escape' => false]);
                echo '<ul class="dropdown-menu" role="menu">';
                foreach ($item['items'] as $lnk => $url) {
                    if($url == 'separator')
                        echo '<li role="separator" class="divider"></li>';
                    else {
                        echo '<li>';
                        echo $this->Html->link(__($lnk), $url['url']);
                        echo '</li>';
                    }
                }
                echo '</ul></li>';
            } else {
                echo '<li>';
                echo $this->Html->link(__($it), $item['url']);
                echo '</li>';
            }
        }
    ?>                  
</ul>