<?php
use Cake\Utility\Inflector;

if (empty($associations['manyToMany'])) {
    $associations['manyToMany'] = [];
}

if (empty($associations['oneToMany'])) {
    $associations['oneToMany'] = [];
}
$relations = array_merge($associations['oneToMany'], $associations['manyToMany']);

$i = 0;
pr($relations);
foreach ($relations as $alias => $details):
    $otherSingularVar = $details['propertyName'];
    ?>
    <div class="related">
        <h3><?= __($alias) ?></h3>
        <?php
        $this->request->here = preg_replace ( '/\/examenes(.*)/' , '${1}' , $this->request->here );
        if (${$viewVar}->{$details['entities']}):
            ?>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th><?= __('Image') ?></th>
                        <th><?= __('Text') ?></th>
                        <th><?= __('Active') ?></th>
                        <th class="actions"><?= __d('crud', 'Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 0;
                        foreach (${$viewVar}->{$details['entities']} as $question) :
                    ?>
                        <tr>
                            <td><?= $question->image?$this->Html->image('questions'.DS. $question->image, ['class' => 'img-responsive', 'style' => 'max-width:150px']):''; ?></td>
                            <td><?= $this->CrudView->process('text', $question); ?></td>
                            <td><?= $question->active?'<span class="label label-success">'.__d('crud','Yes').'</span>':'<span class="label label-danger">'.__d('crud','No').'</span>' ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__d('crud', 'Edit'), array('controller' => 'Questions', 'action' => 'edit', $question['id'], '?' => ['question_id' => $question->question_id , '_redirect_url' => $this->request->here]), ['class' => 'btn btn-xs btn-primary']); ?>
                                <?= $this->Form->postLink(
                                    __d('crud', 'Delete'), 
                                    array('controller' => 'Questions', 'action' => 'delete', $question['id']), 
                                    [
                                        'class' => 'btn btn-xs btn-warning',
                                        'method' => 'DELETE',
                                        'confirm' => 'Confirma eliminar esta pregunta?',
                                        'data' => [
                                            '_redirect_url' => $this->request->here
                                        ]
                                    ]); ?>
                            </td>
                        </tr>
                    <?php
                        endforeach;
                    ?>
                </tbody>
            </table>
            <?php
        endif;
        ?>
    </div>
<?php endforeach; ?>
