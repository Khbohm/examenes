<?= $this->element('view/related/has_one'); ?>
<?php

    if(array_key_exists('oneToMany', $associations)) {
        //if(array_key_exists('Answers', $associations['oneToMany']))
        //    echo $this->element('view/respuestas'); 
        //if(array_key_exists('Questions', $associations['oneToMany']))
        //    echo $this->element('view/preguntas'); 
        $this->element('view/related/has_many'); 
    }
    
    if(array_key_exists('manyToMany', $associations)) {
        if(array_key_exists('Categories', $associations['manyToMany']))
            echo $this->element('view/categorias'); 
    }
    
    else
        echo $this->element('view/related/has_many'); 
?>
