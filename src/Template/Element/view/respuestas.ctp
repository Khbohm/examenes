<?php
use Cake\Utility\Inflector;

if (empty($associations['manyToMany'])) {
    $associations['manyToMany'] = [];
}

if (empty($associations['oneToMany'])) {
    $associations['oneToMany'] = [];
}
$relations = array_merge($associations['oneToMany'], $associations['manyToMany']);

$i = 0;
foreach ($relations as $alias => $details):
    $otherSingularVar = $details['propertyName'];
    ?>
    <div class="related">
        <h3>Respuestas</h3>
        <?php
        $this->request->here = preg_replace ( '/\/examenes(.*)/' , '${1}' , $this->request->here );
        if (${$viewVar}->{$details['entities']}):
            ?>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <?php
                        $otherFields = array_keys(${$viewVar}->{$details['entities']}[0]->toArray());
                        if (isset($details['with'])) {
                            $index = array_search($details['with'], $otherFields);
                            unset($otherFields[$index]);
                        }

                        foreach ($otherFields as $field) {
                            if(!in_array($field, ['id', 'question_id']))
                                echo "<th>" . __(Inflector::humanize($field)) . "</th>";
                        }
                        ?>
                        <th class="actions"><?= __d('crud', 'Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach (${$viewVar}->{$details['entities']} as $answer) :
                        ?>
                        <tr>
                            
                            <td><?= $answer->image?$this->Html->image('answers'.Ds. $answer-image, ['class' => 'img-responsive', 'style' => 'max-width:150px']):''; ?></td>
                            <td><?= $this->CrudView->process('text', $answer); ?></td>
                            <td><?= $answer->correct?'<span class="label label-success">'.__d('crud','Yes').'</span>':'<span class="label label-danger">'.__d('crud','No').'</span>' ?></td>
                            <td><?= $answer->active?'<span class="label label-success">'.__d('crud','Yes').'</span>':'<span class="label label-danger">'.__d('crud','No').'</span>' ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__d('crud', 'Edit'), array('controller' => 'Answers', 'action' => 'edit', $answer['id'], '?' => ['question_id' => $answer->question_id , '_redirect_url' => $this->request->here]), ['class' => 'btn btn-xs btn-primary']); ?>
                                <?= $this->Form->postLink(
                                    __d('crud', 'Delete'), 
                                    array('controller' => 'Answers', 'action' => 'delete', $answer['id']), 
                                    [
                                        'class' => 'btn btn-xs btn-warning',
                                        'method' => 'DELETE',
                                        'confirm' => 'Confirma eliminar esta respuesta?',
                                        'data' => [
                                            '_redirect_url' => $this->request->here
                                        ]
                                    ]); ?>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                </tbody>
            </table>
            <?php
        endif;
        ?>

        <div class="actions">
            <ul>
                <li><?= $this->CrudView->createRelationLink(__($alias), $details);?></li>
            </ul>
        </div>
    </div>
<?php endforeach; ?>
