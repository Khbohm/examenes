<?php
use Cake\Utility\Inflector;

if (empty($associations['manyToMany'])) {
    $associations['manyToMany'] = [];
}

if (empty($associations['oneToMany'])) {
    $associations['oneToMany'] = [];
}
$relations = array_merge($associations['oneToMany'], $associations['manyToMany']);

$i = 0;
foreach ($relations as $alias => $details):
    $otherSingularVar = $details['propertyName'];
    ?>
    <div class="related">
        <h3>Categorias</h3>
        <?php
        $this->request->here = preg_replace ( '/\/examenes(.*)/' , '${1}' , $this->request->here );
        if (${$viewVar}->{$details['entities']}): 
            ?>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <?php
                        //$otherFields = array_keys(${$viewVar}->{$details['entities']}[0]->toArray());
                        $otherFields = ['id', 'name'];
                        if (isset($details['with'])) {
                            $index = array_search($details['with'], $otherFields);
                            unset($otherFields[$index]);
                        }

                        foreach ($otherFields as $field) {
                            if(!in_array($field, ['id', 'question_id']))
                                echo "<th>" . __(Inflector::humanize($field)) . "</th>";
                        }
                        ?>
                        <th class="actions"><?= __d('crud', 'Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach (${$viewVar}->{$details['entities']} as $answer) :
                        ?>
                        <tr>
                            <td><?= $this->CrudView->process('name', $answer); ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__d('crud', 'Edit'), array('controller' => 'Categories', 'action' => 'edit', $answer['id'], '?' => ['test_type_id' => $answer->test_type_id , '_redirect_url' => $this->request->here]), ['class' => 'btn btn-xs btn-primary']); ?>
    
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                </tbody>
            </table>
            <?php
        endif;
        ?>

        <div class="actions">
            <ul>
                <li><?= $this->CrudView->createRelationLink(__($alias), $details);?></li>
            </ul>
        </div>
    </div>
<?php endforeach; ?>
