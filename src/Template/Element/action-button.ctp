<?php
if (!is_array($config)) {
    echo $config;
    return;
}

if ($config['method'] !== 'GET') {
    echo $this->Form->postLink(__d('crud',$config['title']), $config['url'], $config['options']);
    return;
}

echo $this->Html->link(__d('crud', $config['title']), $config['url'], $config['options']);
