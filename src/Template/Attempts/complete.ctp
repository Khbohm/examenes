<?php $this->layout('login'); ?>
<div class="form">

        <?= $this->Form->create($attempt, [
                'url' => ['action' => 'repeat'], 
                'method' => 'POST'
                ]
            ) ?>

        
        <h4 class="text-center">EXAMEN COMPLETO</h4>
        <div class="row">
            <?php if($quedan > 0 ) { ?>
            <div class="col-xs-12 text-center">
                Aún le quedan <?= $quedan ?> intentos para realizar este exámen.
                <br />
                Gracias por confiar en nosotros.
            </div>
            <?= $this->Form->input('email', ['type' => 'hidden', 'value' => $attempt->user->email ]) ?>
            <?= $this->Form->input('code', ['type' => 'hidden', 'value' => $attempt->test->token ]) ?>            

            <div class="col-xs-12 text-center">
                <br><br>
                <?= $this->Form->input('Repetir el examen', ['type' => 'submit', 'class' => 'btn btn-primary btn-lg btn-block', 'templates' => [
                                                      'submitContainer' => '<div class="submit col-xs-12 col-md-4 col-md-offset-4 {{type}}">{{content}}</div>' ]]) ?>
            </div>                                                     
            <?php } else { ?>
            <div class="col-xs-12 text-center">
                Ya no le quedan intentos disponibles. Contactese con la academía si desea intentar nuevamente rendir este examen.
                <br />
                Gracias por confiar en nosotros.
            </div>
            <?php } ?>

        </div>
        <?= $this->Form->end() ?>
</div>
