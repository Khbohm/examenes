<?php $this->layout('login'); ?>

<div class="form ">


        <?= $this->Form->create($attempt) ?>

        <div class="row">
            <?= $this->Form->input('email', ['label' => false, 'placeholder' => 'Ingrese su email', 'class' => 'input-lg', 'templates' => [
                                                      'inputContainer' => '<div class="form-group col-md-4 col-md-offset-4 {{type}}{{required}}">{{content}}<span class="small">Ingrese su email.</span></div>',
                                                      'inputContainerError' => '<div class="form-group col-md-4 col-md-offset-4 {{type}}{{required}} error">{{content}}{{error}}<span class="small">Ingrese su email.</span></div>'
                                                      ] ]) ?>
            <?= $this->Form->input('code', ['label' => false, 
                                            'placeholder' => 'Código de exámen', 
                                            'class' => 'input-lg', 
                                            'templates' => [
                                                'inputContainer' => '<div class="form-group col-md-4 col-md-offset-4 {{type}}{{required}}">{{content}}<span class="small">Ingrese el código otorgado para acceder al exámen.</span></div>',
                                                'inputContainerError' => '<div class="form-group col-md-4 col-md-offset-4 {{type}}{{required}} error">{{content}}{{error}}<span class="small">Ingrese el código otorgado para acceder al examen</span></div>'
                                            ],
                                            'value' => $token
                                            ]) ?>
        
            <?= $this->Form->input('Rendir exámen', ['type' => 'submit', 'class' => 'btn btn-primary btn-lg btn-block', 'templates' => [
                                                      'submitContainer' => '<div class="submit col-xs-12 col-md-4 col-md-offset-4 {{type}}">{{content}}</div>' ]]) ?>
        
        </div>
        <?= $this->Form->end() ?>


</div>

