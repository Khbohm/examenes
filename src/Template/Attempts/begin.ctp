<?php $this->layout('login'); ?>
<div class="form">

        <?= $this->Form->create($attempt) ?>

        <h4 class="text-center">INICIO</h4>
        <div class="row">
            <?= $this->Form->input('id', ['type' => 'hidden']) ?>

            <?= $this->Form->input('Comenzar', ['type' => 'submit', 'class' => 'btn btn-primary btn-lg btn-block', 'templates' => [
                                                      'submitContainer' => '<div class="submit col-xs-12 col-md-4 col-md-offset-4 {{type}}">{{content}}</div>' ]]) ?>

        </div>
        <?= $this->Form->end() ?>
</div>
