<div class="scaffold-action scaffold-action-edit scaffold-controller-questions scaffold-questions-edit">
    <div class="row">
        <div class="col-xs-12">
            <h2><?= __('Edit Question') ?></h2>
            <div class="actions-wrapper">
                <?= $this->Html->link(__d('crud','Back'), ['action' => 'index'], [ "class" => "btn btn-xs btn-default"])  ?>
            </div>
        </div>
    </div>
    <?= $this->Form->create($question, ['type' => 'file' ]) ?>
    <div class="row">
        <div class="col-md-8">
            <?php
                echo $this->Form->input('category_id');
                
                echo $this->Form->input('text');
                
                echo $this->Form->input('active');
            ?>
        </div>
        <div class="col-md-4">
            <label>Imágen</label>
            <?php
                echo $this->Html->image($question->image?'questions/'.$question->image:'https://placehold.it/350x150?text=Sin+imagen', ['class' => 'img-responsive']);
                echo $this->Form->input('image', ['type' => 'file']);
                echo $this->Form->input('image_dir', ['type' => 'hidden']);
            ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <div class="col pull-right">
                    <?= $this->Form->button('Guardar', ['class' => 'btn btn-primary']) ?>
                    <?= $this->Html->link(__d('crud', 'Cancel'), ['action' => 'index'],['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
    </div>

    <?= $this->Form->end() ?>
</div>
