<!DOCTYPE html>
<html lang="<?= \Locale::getPrimaryLanguage(\Cake\I18n\I18n::locale()) ?>">
<head>
    <?= $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $this->get('title');?></title>

    <meta name="description" content="">
    <meta name="author" content="">

    <?= $this->Html->meta('icon'); ?>
    <?= $this->fetch('meta'); ?>
    <?= $this->fetch('css'); ?>
    
    <style type="text/css">
    
        td.actions { white-space: nowrap; }
    
        .scaffold-action h2 {float:left}
        .scaffold-action .actions-wrapper {float:right}
        .scaffold-action hr {clear:both}
        
        th .desc::after {
            position: relative;
            top: 1px;
            left: 1em;
            display: inline-block;
            font-family: 'Glyphicons Halflings';
            font-style: normal;
            font-weight: normal;
            line-height: 1;
            font-size:.9em;
            -webkit-font-smoothing: antialiased;              
            content: "\e156";
        }

        th .asc::after {
            position: relative;
            top: 1px;
            left: 1em;
            display: inline-block;
            font-family: 'Glyphicons Halflings';
            font-style: normal;
            font-weight: normal;
            font-size:.9em;
            line-height: 1;
            -webkit-font-smoothing: antialiased;            
            content: "\e155";
        }
        
    </style>
    
    <?= $this->fetch('headjs'); ?>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="/"><?= $this->Html->image('logo_f_negro.jpg', ['style' => 'width: 39px;margin-top: -9px;']) ?></a>
            </div>
            
            <div id="navbar" class="navbar-collapse collapse">
                <?= $this->Element('menu') ?>
                <?= $this->Element('topbar') ?>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <?php if ($disableSidebar) : ?>
                <div class="col-sm-12">
                    <?= $this->Flash->render(); ?>
                    <?= $this->fetch('content'); ?>
                    <?= $this->fetch('action_link_forms'); ?>
                </div>
            <?php else : ?>
                <div class="col-xs-0 col-sm-2 col-lg-2">
                    <?= $this->element('sidebar'); ?>
                </div>
                <div class="col-xs-12 col-sm-10 col-lg-10">
                    <?= $this->Flash->render(); ?>
                    <?= $this->fetch('content'); ?>
                    <?= $this->fetch('action_link_forms'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?= $this->fetch('script'); ?>
</body>
</html>
