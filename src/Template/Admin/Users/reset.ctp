<?php $this->layout('login') ?>
<div class="users form">
    <?= $this->Form->create($user, ['class' => 'form-signin']) ?>
    <?= $this->Flash->render('auth') ?>
    <?= $this->Flash->render(); ?>
    <h2 class="text-center">Consorcios</h2>
    <h3 class="text-center"><?= __('Please enter your email and a new password') ?></h3>
    <?= $this->Form->input('email', ['label' => ['class' => 'sr-only'], 'placeholder' => __('Email'), 'templates' => ['inputContainer' => '{{content}}', 'inputContainerError' => '{{content}}{{error}}']]) ?>
    <?= $this->Form->input('password', ['label' => ['class' => 'sr-only'], 'class' => 'reset', 'placeholder' => __('Password'), 'templates' => ['inputContainer' => '{{content}}', 'inputContainerError' => '{{content}}{{error}}']]) ?>
    <?= $this->Form->input('password_confirmation', ['type' => 'password', 'label' => ['class' => 'sr-only'], 'class' => 'recover', 'placeholder' => __('Confirm password'), 'templates' => ['inputContainer' => '{{content}}', 'inputContainerError' => '{{content}}{{error}}']]) ?>
    <br />
    <?= $this->Form->button(__('Password reset'), ['class' => 'btn btn-lg btn-primary btn-block']); ?>
    
    <?= $this->Form->end() ?>
</div>
