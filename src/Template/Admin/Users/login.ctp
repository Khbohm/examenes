<?php $this->layout('login') ?>
<div class="users form">
    <?= $this->Form->create(null, ['class' => 'form-signin']) ?>
    <?= $this->Flash->render('auth') ?>
    <?= $this->Flash->render(); ?>

    <h2 class="text-center">EXAMENES</h2>
    <h3 class="text-center"><?= __('Please enter your email and password') ?></h3>
    <?= $this->Form->input('email', ['label' => ['class' => 'sr-only'], 'placeholder' => __('Email'), 'templates' => ['inputContainer' => '{{content}}', 'inputContainerError' => '{{content}}{{error}}']]) ?>
    <?= $this->Form->input('password', ['label' => ['class' => 'sr-only'], 'placeholder' => __('Password'), 'templates' => ['inputContainer' => '{{content}}', 'inputContainerError' => '{{content}}{{error}}']]) ?>
    
    <!--?= $this->Form->input('remember', ['label' => __('Remember me'), 'type' => 'checkbox']) ?-->
    <?= $this->Form->button(__('Login'), ['class' => 'btn btn-lg btn-primary btn-block']); ?>
    <br />
    <?= $this->Html->link(__('I forgot my password'), ['action' => 'recover']) ?>
    <?= $this->Form->end() ?>
</div>
