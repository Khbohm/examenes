<?php $this->layout('login') ?>
<div class="users form">
    <?= $this->Form->create(null, ['class' => 'form-signin']) ?>
    <?= $this->Flash->render('auth') ?>
    <?= $this->Flash->render(); ?>
    <h2 class="text-center">Consorcios</h2>
    <h3 class="text-center"><?= __('Please enter your email') ?></h3>
    <?= $this->Form->input('email', ['label' => ['class' => 'sr-only'], 'class' => 'recover', 'placeholder' => __('Email'), 'templates' => ['inputContainer' => '{{content}}', 'inputContainerError' => '{{content}}{{error}}']]) ?>
    <br />
    <?= $this->Form->button(__('Password recover'), ['class' => 'btn btn-lg btn-primary btn-block']); ?>
    
    <?= $this->Form->end() ?>
</div>
