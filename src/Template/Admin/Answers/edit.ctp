<div class="scaffold-action scaffold-action-edit scaffold-controller-questions scaffold-questions-edit">
    <div class="row">
        <div class="col-xs-12">
            <h2><?= $this->get('title'); ?></h2>
            <div class="actions-wrapper">
                <?= $this->Html->link(__d('crud','Back'), ['action' => 'index', '?' => ['question_id' => $answer->question_id]], [ "class" => "btn btn-xs btn-default"])  ?>
            </div>
        </div>
    </div>
    <?= $this->Form->create($answer, ['type' => 'file' ]) ?>
    <div class="row">
        <div class="col-md-8">
            <?php
                echo $this->Form->input('question_id', ['type' => 'hidden', 'value' => $answer->question_id ]);
                echo $this->Form->input('text');
                
                echo $this->Form->input('correct');
                
                echo $this->Form->input('active');
            ?>
        </div>
        <div class="col-md-4">
            <label><?= h('Imágen') ?></label>
            <?php
                echo $this->Html->image($answer->image?'answers/'.$answer->image:'https://placehold.it/350x150?text=Sin+imagen', ['class' => 'img-responsive']);
                echo $this->Form->input('imageFile', ['type' => 'file']);
            ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <div class="col pull-right">
                    <?= $this->Form->button('Guardar', ['class' => 'btn btn-primary']) ?>
                    <?= $this->Html->link(__d('crud', 'Cancel'), ['action' => 'index', '?' => ['question_id' => $answer->question_id]],['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
    </div>

    <?= $this->Form->end() ?>
</div>
