<?= $this->fetch('before_index'); ?>

<div class="<?= $this->CrudView->getCssClasses(); ?>">

    <?php
    if (!$this->exists('search')) {
        $this->start('search');
            echo $this->element('search');
        $this->end();
    }
    ?>
    
<?php

$actions['table']['add']['url']['?'] = [
    'question_id' => $this->request->query['question_id'],
    '_redirect_url' => DS.$this->request->url.'?question_id='. $this->request->query['question_id']
];
$actions['table']['index'] = [
    'title' => 'Back',
    'url' => [
            'controller' => 'questions',
            'action' => 'index'
        ],
    'method' => 'GET',
    'options' => []
];

//pr($this->request);

//pr($actions['table']['add']['url']);
if (!$this->exists('actions')) {
    $this->start('actions');
        echo $this->element('actions', [
            'actions' => $actions['table'],
            'singularVar' => false,
        ]);
        // to make sure ${$viewVar} is a single entity, not a collection
        if (${$viewVar} instanceof \Cake\Datasource\EntityInterface && !${$viewVar}->isNew()) {
            echo $this->element('actions', [
                'actions' => $actions['entity'],
                'singularVar' => ${$viewVar},
            ]);
        }
    $this->end();
}
?>
<h2><?= $this->get('title'); ?></h2>
<div class="actions-wrapper">
    <?= $this->fetch('actions'); ?>
</div>
    
    

    <?= $this->fetch('search'); ?>

    <hr />

    <?= $this->element('index/bulk_actions/form_start', compact('bulkActions')); ?>

    <div class="table-responsive">
        <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <?= $this->element('index/bulk_actions/table', compact('bulkActions', 'primaryKey', 'singularVar')); ?>

                <?php
                foreach ($fields as $field => $options) :
                    ?>
                    <th>
                        <?php
                        if (!empty($options['disableSort'])) {
                            echo isset($options['title']) ? $options['title'] : \Cake\Utility\Inflector::humanize($field);
                        } else {
                            echo $this->Paginator->sort($field, isset($options['title']) ? $options['title'] : null, $options);
                        }
                        ?>
                    </th>
                    <?php
                endforeach;
                ?>
                <?php if ($actionsExist = !empty($actions['entity'])): ?>
                    <th><?= __d('crud', 'Actions'); ?></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>

            <?php
            foreach (${$viewVar} as $singularVar) :
                ?>
                <tr>
                    <?= $this->element('index/bulk_actions/record', compact('bulkActions', 'primaryKey', 'singularVar')); ?>
                    <?= $this->element('index/table_columns', compact('singularVar')); ?>

                    <?php if ($actionsExist): ?>
                        <td class="actions"><?= $this->element('answersActions', [
                            'singularVar' => $singularVar,
                            'actions' => $actions['entity']
                        ]); ?></td>
                    <?php endif; ?>
                </tr>
                <?php
            endforeach;
            ?>
            </tbody>
        </table>
    </div>

    <?= $this->element('index/bulk_actions/form_end', compact('bulkActions')); ?>
    <?= $this->element('index/pagination'); ?>
</div>

<?= $this->fetch('after_index'); ?>
