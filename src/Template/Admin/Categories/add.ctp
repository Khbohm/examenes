<div class="scaffold-action scaffold-action-edit scaffold-controller-questions scaffold-questions-edit">
    <div class="row">
        <div class="col-xs-12">
            <h2><?= __('Add Category') ?></h2>
            <div class="actions-wrapper">
                <?= $this->Html->link(__d('crud','Back'), ['action' => 'index'], [ "class" => "btn btn-xs btn-default"])  ?>
            </div>
        </div>
    </div>
    <?= $this->Form->create($category) ?>
    <div class="row">
        <div class="col-md-8">
            <?php
                echo $this->Form->input('name');
                
                echo $this->Form->input('description');
                
                echo '<div class="row"><div class="col-xs-6">';
                echo $this->Html->tag('label', __('Test Types'));
                echo '<table class="table table-condensed table-bordered cats">';
                echo $this->Html->tableHeaders(['Ok', __('Test Type'), __('Questions per test')]);
                $kCat = 0;
                foreach ($testTypes as $testTypeId => $testType) {
                    
                    $questions_per_test = 0;
                    $checked = false;
                    /*foreach ($category->test_types as $ttc) {
                        if($ttc->id == $testTypeId) {
                            $questions_per_test = $ttc->_joinData->questions_per_test;
                            $checked = true;
                            break;
                        }
                    }*/
            
                    echo '<tr '.($checked?'class="success"':'') .'>';
                    echo $this->Html->tag('td', $this->Form->input("test_types.$kCat.id", ['value' => $testTypeId, 'type' => 'hidden']).$this->Form->checkbox("categories.$kCat.ok", ['value' => 1, 'type' => 'checkbox', 'checked' => $checked, 'required' => false]));
                    echo $this->Html->tag('td', h($testType));
                    echo $this->Html->tag('td', $this->Form->input("test_types.$kCat._joinData.questions_per_test", ['label' => false, 'value' => $questions_per_test]) );
                    echo '</tr>';
                    
                    //echo $this->Form->input("categories.$kcat.name", ['label' => $category->name, 'value' => $category->id, 'type' => 'checkbox', 'selected' => true]);
                    $kCat ++;
                }
                echo '</table></div></div>';

                echo $this->Form->input('active');
            ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <div class="col pull-right">
                    <?= $this->Form->button(__d('crud', 'Save'), ['class' => 'btn btn-primary']) ?>
                    <?= $this->Html->link(__d('crud','Cancel'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
    </div>

    <?= $this->Form->end() ?>
</div>
