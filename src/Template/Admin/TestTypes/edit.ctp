<div class="scaffold-action scaffold-action-edit scaffold-controller-questions scaffold-questions-edit">
    <div class="row">
        <div class="col-xs-12">
            <h2><?= __('Edit Test Type: {0}', ($testType->name)) ?></h2>
            <div class="actions-wrapper">
                <?= $this->Html->link(__d('crud','Back'), ['action' => 'index'], [ "class" => "btn btn-xs btn-default"])  ?>
            </div>
        </div>
    </div>

<?= $this->Form->create($testType); ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo $this->Form->input('name');
            echo $this->Form->input('description');
            echo $this->Form->input('time', ['append' => 'min.', 'help' => 'Tiempo total para completar el examen. En minutos.']);
            echo $this->Form->input('required', ['append' => '%', 'help' => 'Porcentage de respuestas correctas requerido para aprobar el examen.']);
            //pr($testType);
            echo '<div class="row"><div class="col-xs-6">';
            echo $this->Html->tag('label', 'Categorías');
            echo '<table class="table table-condensed table-bordered cats">';
            echo $this->Html->tableHeaders(['Ok', __('Category'), __('Questions per test')]);
            $kCat = 0;
            foreach ($categories as $categoryId => $category) {
                
                $questions_per_test = 0;
                $checked = false;
                foreach ($testType->categories as $ttc) {
                    if($ttc->id == $categoryId) {
                        $questions_per_test = $ttc->_joinData->questions_per_test;
                        $checked = true;
                        break;
                    }
                }
        
                echo '<tr '.($checked?'class="success"':'') .'>';
                echo $this->Html->tag('td', $this->Form->input("categories.$kCat.id", ['value' => $categoryId, 'type' => 'hidden']).$this->Form->checkbox("categories.$kCat.ok", ['value' => 1, 'type' => 'checkbox', 'checked' => $checked, 'required' => false]));
                echo $this->Html->tag('td', h($category));
                echo $this->Html->tag('td', $this->Form->input("categories.$kCat._joinData.questions_per_test", ['label' => false, 'value' => $questions_per_test]) );
                echo '</tr>';
                
                //echo $this->Form->input("categories.$kcat.name", ['label' => $category->name, 'value' => $category->id, 'type' => 'checkbox', 'selected' => true]);
                $kCat ++;
            }
            echo '</table></div></div>';
            
            echo $this->Form->input('active');
            ?>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <div class="col pull-right">
                    <?= $this->Form->button(__d('crud', 'Save'), ['class' => 'btn btn-primary']) ?>
                    <?= $this->Html->link(__d('crud','Cancel'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
    </div>

<?= $this->Form->end() ?>
<br />
<br />
<?php $this->Html->scriptStart(['block' => true]); ?>
$(".cats input[type='checkbox']").click(function(evt){
    
    tr = $(this).parents('tr');
    if( $(this).prop("checked") ) {
        tr.addClass('success');
    } else {
        tr.removeClass('success');
    }
});

<?php $this->Html->scriptEnd(); ?>