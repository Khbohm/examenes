<?php
    $this->layout('reply'); 
    $percent = $number / $total * 100;
    
    $submitLabel = __('Next');
    if($number==$total) $submitLabel = __('Finish');
?>
<div class="row" style="margin-top:50px">
    <div class="col-xs-12">
        <h4>
            <?= $number ?> - <?= h($question->text) ?>
            <?php if(!empty($question->image)) {echo $this->Html->image($question->image, ['class' => 'img-responsive']);} ?>
        </h4>
    </div>
    <?= $this->Form->create($reply) ?>
    <?= $this->Form->input('id', ['type' => 'hidden', 'value'=>$reply->id]) ?>
    <div class="col-xs-12">
        <div class="well">
            <?php foreach($question->answers as $key => $answer) { ?>
            <div class="radio">
              <label>
                <input type="radio" name="answer_id" id="answer_id<?= $key ?>" value="<?= $answer->id ?>">
                    <?php if (!empty($answer->image)) echo $this->Html->image($answer->image, ['class' => 'img-responsive']); ?>
                    <?= h($answer->text) ?>
              </label>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="col-xs-6 col-xs-offset-6">
        <?= $this->Form->button($submitLabel, ['type' => 'submit', 'class' => 'btn btn-success pull-right', 'style' => 'display:none']); ?>
    </div>
    <?= $this->Form->end() ?>
</div>
<?php $this->Html->scriptStart(['block' => true]); ?>
$("input[type='radio']").on('click', function(evt){$("button[type='submit']").show();});

jQuery.timeago.settings.allowFuture = true;

setTimeout(function(){ $("time.timeago").timeago(); }, 1000);

<?php $this->Html->scriptEnd(); ?>