<?php $this->layout('rendir'); ?>
<div class="container-fluid ">

    <div class="jumbotron back">

        <h3 class="text-center">Evaluación en línea de conocimientos teóricos.</h3>
        <div class="row">
        HOME    
        <br>
            <div class=" col-md-offset-4 col-md-4">
                <?=  $this->Html->link(h('Rendir exámen'), ['controller' => 'attempts', 'action' => 'add' ], ['class' => 'btn btn-block btn-lg btn-success'])?>
            </div>
        </div>

    </div>
</div>

