<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * Replies Controller
 *
 * @property \App\Model\Table\RepliesTable $Replies
 */
class RepliesController extends AppController
{
    public function add() {
        
        $session = $this->request->session();
        $attempt = $session->read('attempt');
        
        $total = count($attempt->replies);
        $number =  $session->read('reply_number');

        if($this->request->is(['put', 'post'])) {

            $reply = $attempt->replies[$number];

            $reply->replied_on = Time::now();
            $reply = $this->Replies->patchEntity($reply, $this->request->data);
            
            if($this->Replies->save($reply)) {

                $number ++;

                if($number == $total) {
                    return $this->redirect(['controller' => 'attempts', 'action' => 'complete' ]);
                }

                $session->write('reply_number', $number);
                
            } else {
                $this->Flash->error('Por favor, revise su respuesta.');
            }

        } else {
            $this->Replies->Attempts->loadInto($attempt, ['Replies']);    
            
            $session = $this->request->session();
            $session->write('attempt', $attempt);
            
            
            $session->write('reply_number', 0);

        }

        $reply = $attempt->replies[$number];


        $question = $this->Replies->Questions->get($reply->question_id, ['contain' => ['Answers']]);
     
        $number++;
     
        $this->set(compact('total', 'number', 'question', 'reply', 'attempt'));
    }
}
