<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tests Controller
 *
 * @property \App\Model\Table\TestsTable $Tests
 */
class TestsController extends AppController {
    
     public function add() {
        $action = $this->Crud->action(); // Gets the IndexAction object
        
        pr($this->request);
        
        /*
        $action->config('scaffold.fields', [
            'firstname', 
            'lastname', 
            'email', 
            'password',
            'password_confirmation' => ['type' => 'password'], 
            'role' => [
                'options' => ['user' => 'Usuario', 'admin' => 'Administrador'],
                'empty' => __('Select a role')
            ],
            'active'
        ]);
        
        $action->config('scaffold.extra_buttons_blacklist', [
            'save_and_continue', // Hide the Save and Continue button
            //'save_and_create', // Hide the Save and create new button
            //'back', // Hide the back button
        ]);
        
        $action->config('scaffold.actions', ['index' => ['link_title' => 'Back']]);
        
        $action->config('scaffold.page_title', __('Add user')); 

        $action->config('scaffold.disable_sidebar', true);       */
        return $this->Crud->execute();
    }  
    
}
