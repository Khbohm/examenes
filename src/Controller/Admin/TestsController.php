<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Routing\Router;

/**
 * Tests Controller
 *
 * @property \App\Model\Table\TestsTable $Tests
 */
class TestsController extends AppController
{

    public function index() {
        $action = $this->Crud->action(); // Gets the IndexAction object
        
        $action->config('scaffold.relations', ['TestTypes', 'Users', 'Attempts']);
        
        $action->config('scaffold.fields', [
            'user_id',
            'test_type_id',
            'attempts' => [
                'title' => __('Attempts made'),
                'formatter' => function ($name, $value) {
                    
                        return count($value);
                    }
            ],
            'allowed_attempts',
            'active',
            'modified'
        ]);
        
        $action->config('scaffold.actions', ['view', 'edit', 'delete', 'add']);

        $action->config('scaffold.disable_sidebar', true);       
        $action->config('scaffold.page_title', __('Tests'));       
        
        return $this->Crud->execute();
    }  
    
    
    public function add() {
        $action = $this->Crud->action(); // Gets the IndexAction object
        
        $fields = [
            'user_id',
            'test_type_id',
            'allowed_attempts' => ['templates' => ['input' => '<input type="number" min="1" value="1" name="{{name}}" type="{{type}}" {{attrs}} >']],
            'active_for' => ['help' => 'Límite de tiempo para realizar los exámenes.', 'append' => 'hs.', 'value' => 72],
            'active'
        ];
        
        if(array_key_exists('user_id', $this->request->query)) {
            
            $user_id = $this->request->query['user_id'];
            
            $fields = [
                'user_id' => ['type' => 'hidden', 'templates' => ['input' => '<input value="'.$user_id.'" name="{{name}}" type="{{type}}" {{attrs}} >']],
                'test_type_id',
                'allowed_attempts' => ['templates' => ['input' => '<input min="1" value="1" name="{{name}}" type="{{type}}" {{attrs}} >']],
                'active_for' => ['help' => 'Límite de tiempo para realizar los exámenes.', 'append' => 'hs.', 'value' => 72 ],
                'active'
            ];
            
            if(array_key_exists('_redirect_url', $this->request->query)) {
                $fields['_redirect_url'] = ['type' => 'hidden', 'templates' => ['input' => '<input value="'.$this->request->query['_redirect_url'].'" name="{{name}}" type="{{type}}" {{attrs}} >']];
                unset($this->request->query['_redirect_url']);
            };

            $user = $this->Tests->Users->get($user_id);
            
            $action->config('scaffold.page_title', __('Add test for {0}', $user->fullname));   
        } else {
            $action->config('scaffold.page_title', __('New Test'));   
        }
        
        $fields['msg'] = [
            'type' => 'textarea', 
            'label' => 'Mensaje del correo',
            'value' => 'La academia de manejo le ha habilitado la realización de un exámen.'."\n".'Para completarlo acceda a la dirección: [*DIRECCION*] he ingrese el siguiente código [*CODIGO*]',
            'help' => 'Texto del correo que se enviará al alumno. Las etiquetas [*DIRECCION*] y [*CODIGO*] serán reemplazadas por los datos correspondientes al momento de enviar el correo.'
        ];
        
        $action->config('scaffold.fields', $fields);
        
        $this->Crud->on('relatedModel', function(\Cake\Event\Event $event) {
            if ($event->subject->association->name() === 'TestTypes') {
                $event->subject->query->where(['active' => true]);
            };
               
            if ($event->subject->association->name() === 'Users') {
                $event->subject->query->where(['role' => 'user']);
            };
        });  
        
        $this->Crud->on('beforeSave',function(\Cake\Event\Event $event) {
            $event->subject->entity->token = $this->Tests->createToken();
        });
        
        $this->Crud->on('beforeRedirect', [$this, '_beforeAddRedirect']);

        $this->Crud->on('afterSave',function(\Cake\Event\Event $event) {
            
            if($event->subject->entity->active) {
                
                $lnk = Router::url(['controller' => 'rendir' ], true);
                
                $msg = sprintf('La academia de manejo le ha habilitado la realización de un exámen. Para completarlo acceda a la dirección: %s he ingrese el siguiente código %s', $lnk, $event->subject->entity->token);
                
                $user = $this->Tests->Users->get($event->subject->entity->user_id);
                
                //Enviamos un email al usuario informandole;
                $email = new Email('mailgun');
                $email->from(['me@example.com' => 'Academia de Manejo'])
                    ->to([$user->email => $user->fullname])
                    ->subject('Exámen habilitado')
                    ->send($msg);            
            }
        });


        $action->config('scaffold.extra_buttons_blacklist', [
            'save_and_continue', // Hide the Save and Continue button
            'save_and_create', // Hide the Save and create new button
            //'back', // Hide the back button
        ]);

        $action->config('scaffold.actions', ['index' => ['link_title' => 'Back']]);

        $action->config('scaffold.disable_sidebar', true); 
        return $this->Crud->execute();
    }   
    
    public function _beforeAddRedirect(\Cake\Event\Event $event) {
    
        if(isset($event->subject()->entity->_redirect_url))
            $event->subject()->url = ['controller' => 'Users', 'action' => 'index', 'prefix' => 'admin'];
    
        //pr($event->subject()); die;
        //$question_id = $event->subject()->entity->id;
        
        //
    
    }    
    
    public function edit() {
        $action = $this->Crud->action(); // Gets the IndexAction object
        
        $action->config('scaffold.fields', [
            'test_type_id',
            'allowed_attempts',
            'active_for' => ['help' => 'Límite de tiempo para realizar los exámenes.', 'append' => 'hs.'],
            'active'
        ]);
        
        $fields = [
            'user_id',
            'test_type_id',
            'allowed_attempts' => ['templates' => ['input' => '<input type="number" min="1" value="1" name="{{name}}" type="{{type}}" {{attrs}} >']],
            'active_for' => ['help' => 'Límite de tiempo para realizar los exámenes.', 'append' => 'hs.'],
            'active'
        ];
        
        $this->Crud->on('afterFind',function(\Cake\Event\Event $event) use($action) {
            $action->config('scaffold.page_title', __('Edit {0}\'s test', $event->subject->entity->user->fullname ));
        });
        
        $action->config('scaffold.extra_buttons_blacklist', [
            'save_and_continue', // Hide the Save and Continue button
            'save_and_create', // Hide the Save and create new button
            //'back', // Hide the back button
        ]);

        $this->Crud->on('afterSave',function(\Cake\Event\Event $event) {
            
            if($event->subject->entity->active && !$event->subject->entity->getOriginal('active')) {
                $user = $this->Tests->Users->get($event->subject->entity->user_id);
                
                //Enviamos un email al usuario informandole;
                $email = new Email('mailgun');
                $email->from(['me@example.com' => 'Academia de Manejo'])
                    ->to([$user->email => $user->fullname])
                    ->subject('Exámen habilitado')
                    ->send('La academia de manejo le ha habilitado la realización de un exámen. Para completarlo acceda al siguiente link.');            
            }
        });


        
        $action->config('scaffold.actions', ['index' => ['link_title' => 'Back']]);
        

        $action->config('scaffold.disable_sidebar', true); 
        return $this->Crud->execute();
    }         
    
}
