<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Routing\Router;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    
   public function initialize()
    {
        parent::initialize();
        
        $this->Crud->mapAction('deleteAll', 'Crud.Bulk/Delete');        
      
        
        $this->loadComponent('Cookie');
        
    }

    public function beforeFilter(Event $event)  {
        
        if (!array_key_exists('prefix', $this->request->params) || $this->request->params['prefix'] !== 'admin') {
            $this->Auth->allow(['login', 'recover', 'reset']);
        }            

//pr($event);

        if($this->request->params['action'] == 'index') {
            $this->Crud->action()->config('scaffold.bulk_actions', [
               Router::url(['action' => 'deleteAll']) => __('Delete selected')
            ]);
        }

        return parent::beforeFilter($event);
    }

    
    public function login() {
        if ($this->request->is('post')) {
            
            $user = $this->Auth->identify();
            if ($user) {

                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid email or password, try again'));
        }

    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }    
    
    public function recover() {
        if ($this->request->is('post')) {
            
            $user = $this->Users->findByEmail($this->request->data['email'])->first();
            
            if ($user) {
                
                $resetToken = $this->Users->createToken();
                
                $user->reset_token = $resetToken;
                $user->reseted_at = Time::now();
                
                if($this->Users->save($user)) {
                    
                    $resetUrl = Router::url(['controller' => 'users', 'action' => 'reset', '?' => [ 't' => $user->reset_token]], true);
                    
                    $message = __("Go to the address: {0} to restart your password?.", $resetUrl);
                    
                    //Send email with a code
                    $email = new Email();
                    $email->from(['me@example.com' => 'Consorcios'])
                        ->to($user->email)
                        ->subject('Password reset');

                    $result = $email->send($message);
                    debug($result);
                        
                    $this->Flash->success(__('Check your email for instructions for reset your password.'));
                    
                    $this->render('login');
                }
                
            }
            $this->Flash->error(__('Invalid email, try again'));
        }
    }
    
    public function reset() {
        
        if($this->request->is('post')) {
            $user = $this->Users->findByEmail($this->request->data['email'])->first();
            
            $id = $user->id;
            
            if($user && $user->reset_token == $this->request->query('t') ){
                
                $user = $this->Users->newEntity( array_merge($user->toArray(), $this->request->data) );
                
                $user->id = $id;

                if ($this->Users->save($user)) {
                    $this->Flash->success(__('The password has been reseted.'));
                    $this->Auth->setUser($user->toArray());
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Flash->error(__('The password could not be reseted. Please, try again.'));
                    
                    
                }
            }
        } else {
            
            if(!$this->request->query('t')) {
                $this->loadAction('login');
            }
            
            $reset_token = $this->request->query('t');
            
            $user = $this->Users->findByResetToken($reset_token)->first();
            
            if($user) {
                if(!$user->reseted_at->wasWithinLast('1 hour')) {
                    $this->Flash->error(__('The reset token has expired.'));
                    $this->loadAction('recover');
                } 
                
                $user = $this->Users->newEntity();
            }
            
        }
        
        $this->set(compact('user'));
        
    
    }
    
    public function index() {
        $action = $this->Crud->action(); // Gets the IndexAction object
        
        
        $this->_scaffoldFields($action);
        
        $action->config('scaffold.actions', ['view', 'edit', 'delete', 'create_test', 'add']);

        $action->config('scaffold.disable_sidebar', true);       
        $action->config('scaffold.page_title', __('Users'));       
        
        return $this->Crud->execute();
    }  

    public function view() {
        $action = $this->Crud->action(); // Gets the IndexAction object
        
        $this->_scaffoldFields($action);
        
        $action->config('scaffold.actions', ['edit', 'delete', 'index' => ['link_title' => 'Back']]);
        $action->config('scaffold.disable_sidebar', true);     
        
        $action->config('scaffold.page_title', __('User detail')); 

        return $this->Crud->execute();
    }  
    
    public function edit() {
        $action = $this->Crud->action(); // Gets the IndexAction object

        $fields = [
            'firstname', 'lastname', 'email', 'role' => [
                'options' => ['user' => 'Usuario', 'admin' => 'Administrador'],
                'empty' => __('Select a role')
            ], 'active'];

        if($this->request->params['pass'][0] == $this->Auth->User('id')) {
            $fields = [
                'firstname',
                'lastname',
                'password' => ['value' => ''],
                'password_confirmation' => ['type' => 'password']
                ];
        }
        
        $action->config('scaffold.fields', $fields);
        
         $action->config('scaffold.extra_buttons_blacklist', [
            'save_and_continue', // Hide the Save and Continue button
            'save_and_create', // Hide the Save and create new button
            //'back', // Hide the back button
        ]);
        
        $action->config('scaffold.actions', ['index' => ['link_title' => 'Back']]);

        $action->config('scaffold.disable_sidebar', true);   

        $action->config('scaffold.page_title', __('Edit user')); 
        
        
        return $this->Crud->execute();
    }  
    
    public function add() {
        $action = $this->Crud->action(); // Gets the IndexAction object
        
        $action->config('scaffold.fields', [
            'firstname', 
            'lastname', 
            'email', 
            'password',
            'password_confirmation' => ['type' => 'password'], 
            'role' => [
                'options' => ['user' => 'Usuario', 'admin' => 'Administrador'],
                'empty' => __('Select a role')
            ],
            'active'
        ]);
        
        $action->config('scaffold.extra_buttons_blacklist', [
            'save_and_continue', // Hide the Save and Continue button
            //'save_and_create', // Hide the Save and create new button
            //'back', // Hide the back button
        ]);
        
        $action->config('scaffold.actions', ['index' => ['link_title' => 'Back']]);
        
        $action->config('scaffold.page_title', __('Add user')); 

        $action->config('scaffold.disable_sidebar', true);       
        return $this->Crud->execute();
    }  
    
    public function createTest($user_id = null){
        if( is_null($user_id)  ) $this->setAction('index');
        
        return $this->redirect([
            'prefix' => 'admin', 
            'controller' => 'Tests', 
            'action' => 'add', 
            '?' => [
                'user_id' => $user_id, 
                '_redirect_url' => Router::url([
                    'prefix' => 'admin', 
                    'controller' => 'Users', 
                    'action' => 'index'
                ]) 
            ]
        ]);
    }
    
    protected function _scaffoldFields(&$action) {
        
        
        
        $action->config('scaffold.fields', [
            'fullname',
            'email', 
            'role' => [
                'formatter' => function ($name, $value, $entity) {
                                    $roles = ['user' => 'Usuario', 'admin' => 'Administrador'];
                                    return $roles[$value];
                                }
            ],
            'active'
        ]);
    }
    
}
