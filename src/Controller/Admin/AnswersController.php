<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Routing\Router; 

/**
 * Answers Controller
 *
 * @property \App\Model\Table\AnswersTable $Answers
 */
class AnswersController extends AppController
{
    
    public function question($id) {
        $answer = $this->Answers->get($id);
        return $this->redirect(
            ['controller' => 'Questions', 'action' => 'view', $answer->question_id]
        );
    }
    
    public function index() {
        
        $action = $this->Crud->action(); // Gets the IndexAction object
        
        $action->config('scaffold.fields', [
            'image'=> [
                'formatter' => function ($name, $value, $entity) {
                                    if($value)
                                        return '<img class="img-responsive" src="'.Router::url('/').'img'.DS.'answers'.DS.$value.'" style="max-width: 100px;"/>';
                                    return '';
                                }
            ],
            'text', 
            'active',
            'correct'
        ]);


        $action->config('scaffold.disable_sidebar', true);

        $this->Crud->on('beforePaginate', function ($event) {
            $paginationQuery  = $event->subject()->query;
            $paginationQuery->where([
                'Answers.question_id' => $this->request->query['question_id']
            ]);
        });        
        
        $this->Crud->on('afterPaginate', function ($event) use ($action) {
            $question = $this->Answers->Questions->get($this->request->query['question_id']);
            $title = '<br />'. $question->text;
            if($question->image){
                $html = new \Cake\View\Helper\HtmlHelper(new \Cake\View\View());
                $title .= '&nbsp;'. $html->image('questions'.DS.$question->image, ['style' => 'max-width:100px;max-height:60px;']);
            }

            $action->config('scaffold.page_title', __('Answers of {0}',  $title ));
        });        
        
        
        return $this->Crud->execute();
    }    
    
    
    public function edit(){
        
        $action = $this->Crud->action();
        $action->config('scaffold.disable_sidebar', true); 
        $action->config('scaffold.fields', [
            'question_id' => ['type' => 'hidden'],
            'text',
            'image',
            'correct',
            'active'
            
        ]);         
        $action->config('scaffold.disable_extra_buttons', true);
        

        $action->config('scaffold.actions', [
            'question' => ['link_title' => 'Back']
        ]);        

        $question = $this->Answers->get($this->request->params['pass'][0], ['contain' => ['Questions']])->question;
        $title = '<br />'. $question->text;
        if($question->image){
            $html = new \Cake\View\Helper\HtmlHelper(new \Cake\View\View());
            $title .= '&nbsp;'. $html->image('questions'.DS.$question->image, ['style' => 'max-width:100px;max-height:60px;']);
        }
        $action->config('scaffold.page_title', __('Edit Answer for {0}',  $title ));
 

        $this->Crud->on('beforeSave', function(\Cake\Event\Event $event) {
            if($event->subject->entity->dirty('imageFile')) {
                $file = $event->subject->entity->imageFile;
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png');
                $newName =  WWW_ROOT . 'img/answers/' . $file['name'];

                if (in_array($ext, $arr_ext)) {
                    move_uploaded_file($file['tmp_name'],$newName);
                };
                $event->subject->entity->image = $file['name'];
            };
        });
        
                

        return $this->Crud->execute();
    }
   
    public function add(){
        
        $this->Crud->on('beforeSave', function(\Cake\Event\Event $event) {
            if($event->subject->entity->dirty('imageFile')) {
                $file = $event->subject->entity->imageFile;
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png');
                $newName =  WWW_ROOT . 'img/answers/' . $file['name'];

                if (in_array($ext, $arr_ext)) {
                    move_uploaded_file($file['tmp_name'],$newName);
                };
                $event->subject->entity->image = $file['name'];
            };
        });          

        $action = $this->Crud->action();
        
        $action->config('scaffold.disable_extra_buttons', true);
        
        $action->config('scaffold.disable_sidebar', true); 
        $action->config('scaffold.fields', [
            'question_id' => ['type' => 'hidden'],
            'text',
            'image' => ['type' => 'file'],
            'correct',
            'active'
            
        ]); 
        $action->config('scaffold.disable_extra_buttons', true);
        
        $question = $this->Answers->Questions->get($this->request->query['question_id']);
        $this->set('question_id', $this->request->query['question_id']);
        $title = '<br />'. $question->text;
        if($question->image){
            $html = new \Cake\View\Helper\HtmlHelper(new \Cake\View\View());
            $title .= '&nbsp;'. $html->image('questions'.DS.$question->image, ['style' => 'max-width:100px;max-height:60px;']);
        }
        $action->config('scaffold.page_title', __('New Answer for {0}',  $title ));

        return $this->Crud->execute();
    }    
    
  
}
