<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * TestTypes Controller
 *
 * @property \App\Model\Table\TestTypesTable $TestTypes
 */
class TestTypesController extends AppController
{
    public function index() {
        $action = $this->Crud->action(); // Gets the IndexAction object
        
        $action->config('scaffold.fields', [
            'name',
            'time' => [
                'title' => 'Tiempo para resolverlo',
                'formatter' => function ($name, $value, $entity) {
                                    return $value . ' mins.';
                                }
            ],
            'required' => [
                'title' => 'Requerido',
                'formatter' => function ($name, $value, $entity) {
                                    return number_format($value, 2, ',', '.').' %';
                                }
            ],
            'active'
        ]);
        
        $action->config('scaffold.actions', ['view', 'edit', 'delete', 'add']);

        $action->config('scaffold.disable_sidebar', true);       
        $action->config('scaffold.page_title', __('Test Types'));       
        
        return $this->Crud->execute();
    }
    
    public function add(){
        $action = $this->Crud->action();
        $action->config('scaffold.disable_sidebar', true);
        $action->config('scaffold.fields', [
            'name',
            'description',
            'time' => [
                'label' => 'Tiempo para resolver el examen', 
                'help' => 'Indicar la cantidad de minutos.'
            ],
            'categories._ids' => [
                'help' => 'Podrá indicarlas luego.'
            ]
        ]);
        
        $this->Crud->on('beforeSave', function ($event) use ($action) {
            //pr(  $event->subject()->entity);die;
            foreach ($event->subject()->entity->categories as $k => $category) {
                if($category->ok == 0) {
                    unset($event->subject()->entity->categories[$k]);
                }
            }
        });         
        
        $action->config('scaffold.extra_buttons_blacklist', ['save_and_continue', 'save_and_create' ]);
        
        return $this->Crud->execute();
    }    
    
    public function edit(){
        $action = $this->Crud->action();
        $action->config('scaffold.disable_sidebar', true);

 


        $action->config('scaffold.extra_buttons_blacklist', ['save_and_continue', 'save_and_create' ]);
        
        $this->Crud->on('beforeSave', function ($event) use ($action) {
            //pr(  $event->subject()->entity);die;
            foreach ($event->subject()->entity->categories as $k => $category) {
                if($category->ok == 0) {
                    unset($event->subject()->entity->categories[$k]);
                }
            }
        });                 
        
        $this->Crud->on('afterFind', function ($event) use ($action) {
            $action->config('scaffold.page_title', __('Test Type edit: {0}',  $event->subject()->entity->name ));
        });         
        
        return $this->Crud->execute();
    } 
    
    public function view(){
        $action = $this->Crud->action();
        $action->config('scaffold.disable_sidebar', true);
        
        $action->config('scaffold.fields', [
            'name',
            'description',
            'time' => [
                'title' => 'Tiempo para resolver el examen'
            ],
            'required' => [
                'label' => 'Porcentaje de respuestas requerido para aprobar.'
            ],
            'created'
        ]);        
        
        $this->Crud->on('beforeFind', function ($event) {
            $event->subject()->query->contain([
                'Categories' => [
                    'fields' => [
                        'id', 
                        'name', 
                        'active', 
                        'CategoriesTestTypes.test_type_id']
                    ]
                ]);
            
        });    
        
        $action->config('scaffold.actions_blacklist', ['add']);
        
        $this->set('fromJoinData', 'questions_per_test');
        
        $this->Crud->on('afterFind', function ($event) use ($action) {
            $action->config('scaffold.page_title', __('Test Type: {0}',  $event->subject()->entity->name ));
        });         
        
        //$action->config('scaffold.fields_blacklist', ['created', 'modified', 'image', 'categories._ids']);

        return $this->Crud->execute();
    }    
    
}
