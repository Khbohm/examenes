<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Routing\Router; 

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 */
class CategoriesController extends AppController
{
    
    
    public function index() {
        $action = $this->Crud->action(); // Gets the IndexAction object
        
        $action->config('scaffold.fields', [
            'name',
            'active'
        ]);
        
        $action->config('scaffold.actions', ['view', 'edit', 'delete', 'add']);

        $action->config('scaffold.disable_sidebar', true);       
        $action->config('scaffold.page_title', __('Categories'));       
        
        return $this->Crud->execute();
    }  
    
    public function edit(){
        $action = $this->Crud->action();
        $action->config('scaffold.fields', [
            'name', 
            'description', 
            'test_types._ids' => [
                'help' => 'Tipos de exámen en lo que se incluye esta categoría.'
            ], 
            'active'
        ]);
        $action->config('scaffold.disable_sidebar', true); 


        $this->Crud->on('afterFind', function ($event) use ($action) {
            $action->config('scaffold.page_title', __('Edit Category: {0}',  $event->subject()->entity->name ));
        });         

        $this->Crud->on('beforeSave', function ($event) use ($action) {
            foreach ($event->subject()->entity->test_types as $k => $test_type) {
                if($test_type->ok == 0) {
                    unset($event->subject()->entity->test_types[$k]);
                }
            }
        });  

        return $this->Crud->execute();
    }
    
    public function add() {
        $action = $this->Crud->action();
        $action->config('scaffold.disable_sidebar', true);
        $action->config('scaffold.fields_blacklist', ['created', 'modified', 'image']);

        $this->Crud->on('beforeSave', function ($event) use ($action) {
            foreach ($event->subject()->entity->test_types as $k => $test_type) {
                if($test_type->ok == 0) {
                    unset($event->subject()->entity->test_types[$k]);
                }
            }
        });  
        
        return $this->Crud->execute();
    }
    
    public function view(){
        $action = $this->Crud->action();
        $action->config('scaffold.disable_sidebar', true);
        
        $action->config('scaffold.fields_blacklist', ['created', 'modified', 'image', 'test_types._ids']);
        
        $action->config('scaffold.actions_blacklist', ['add']);
        
        $this->Crud->on('afterFind', function ($event) use ($action) {
            $action->config('scaffold.page_title', __('Category: {0}',  $event->subject()->entity->name ));
        });         
        
        
        $this->Crud->on('beforeFind', function ($event) {
            $event->subject()->query->contain([
                'Questions' => [
                    'fields' => [
                        'image',
                        'text',
                        'active',
                        'id',
                        'category_id'
                    ]
                ],
                'TestTypes' => [
                    'fields' => [
                        'id', 
                        'name', 
                        'CategoriesTestTypes.category_id'
                        ]
                ]
                
            ]);
        });        
        
        $this->set('fromJoinData', 'questions_per_test');
        
        return $this->Crud->execute();
    }
    
}
