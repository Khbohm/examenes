<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Routing\Router; 

/**
 * Questions Controller
 *
 * @property \App\Model\Table\QuestionsTable $Questions
 */
class QuestionsController extends AppController
{
    public function index() {
        $action = $this->Crud->action(); // Gets the IndexAction object
        
        $action->config('scaffold.fields', [
            'image'=> [
                'formatter' => function ($name, $value, $entity) {
                                    if($value)
                                        return '<img class="img-responsive" src="'.Router::url('/').'img'.DS.'questions'.DS.$value.'" style="max-width: 100px;"/>';
                                    return '';
                                }
            ],
            'text', 
            'active'
        ]);
        
        $action->config('scaffold.actions', ['view', 'edit', 'delete', 'add', 'answers' => ['class' => 'btn btn-xs btn-info']]);

        $action->config('scaffold.disable_sidebar', true);
        $action->config('scaffold.page_title', __('Questions'));       
        
        return $this->Crud->execute();
    }
    
    public function answers($question_id) {
        return $this->redirect(
            ['controller' => 'Answers', 'action' => 'index', '?' => [ 'question_id' => $question_id ]]
        );
    }
    
    
    public function edit(){
        $action = $this->Crud->action();
        $action->config('scaffold.relations', ['Categories']);
        $action->config('scaffold.disable_sidebar', true); 

        return $this->Crud->execute();
    }
   
    public function add() {
    
        $action = $this->Crud->action();
        $action->config('scaffold.relations', ['Categories']);
        $action->config('scaffold.disable_sidebar', true);
        
        $this->Crud->on('beforeRedirect', [$this, '_beforeAddRedirect']);
        
        return $this->Crud->execute();
    }
    

    public function _beforeAddRedirect(\Cake\Event\Event $event) {
    
        $question_id = $event->subject()->entity->id;
        
        $event->subject()->url = ['controller' => 'Answers', 'action' => 'index', '?' => [ 'question_id' => $question_id ]];
    
    }
   

    public function view(){
        $action = $this->Crud->action();
        $action->config('scaffold.disable_sidebar', true);
        $action->config('scaffold.fields', [
            'text', 
            'image'=> [
                'formatter' => function ($name, $value, $entity) {
                                    if($value)
                                        return '<img class="img-responsive" src="'.Router::url('/').'img'.DS.'questions'.DS.$value.'" />';
                                    return '';
                                }
            ],
            'active'
        ]);        
        $action->config('scaffold.actions', ['edit', 'delete', 'index' => ['link_title' => 'Back']]);
        $this->Crud->on('beforeFind', function ($event) {
            $event->subject()->query->contain([
                'Answers' => ['fields' => ['id', 'question_id', 'image', 'text', 'correct', 'active']]
            ]);
        });
        
        return $this->Crud->execute();
    }
    
    
}
