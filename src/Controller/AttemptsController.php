<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Core\Configure;

/**
 * Attempts Controller
 *
 * @property \App\Model\Table\AttemptsTable $Attempts
 */
class AttemptsController extends AppController
{

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($token = null) {
        
        $attempt = $this->Attempts->newEntity();
        
        if ($this->request->is('post')) {
        
            $email = $this->request->data('email');
            $token = $this->request->data('code');
            
            
            if($this->request->session()->check('user'))
                $user = $this->request->session()->read('user');
            else {
                //Buscamos el Usuario por el email
                $user = $this->Attempts->Tests->Users->findByEmail($email)->contain([
                    'tests'  => [
                        'Attempts',
                        'TestTypes.Categories',
                        'queryBuilder' => function ($q) use ($token) {
                            return $q->where(['tests.token' => $token]);
                        }
                    ]
                ])->first();

            
                if(!$user) {
                    $this->Flash->error('El correo que ha ingresado no corresponde a un alumno inscripto. Por favor, revíselo.');
    
                    return $this->redirect(['controller' => 'Attempts',  'action' => 'add']);
                }
            }

            if (count($user->tests)==0) {
                $this->Flash->error('El código ingresado no corresponde a ningún exámen. Por favor, revíselo he intente nuevamente.');
                               
                return $this->redirect('/');
            }


            if(!$user->tests[0]->active) {
                $this->Flash->error('El exámen no está activo. Por favor, contacte con el instructor.');
                
                return $this->redirect('/');
            }

            if(!$user->tests[0]->modified->wasWithinLast($user->tests[0]->active_for.' hours')) {
                $this->Flash->error('El tiempo para rendir el examen ha caducado. Por favor, contacte con el instructor.');
                
                return $this->redirect('/');
            }
            

            
            if(count($user->tests[0]->attempts) == $user->tests[0]->allowed_attempts ) {
                $this->Flash->error('Ya ha utilizado todos los intentos disponibles para este exámen. Por favor, contacte con el instructor.');
                
                return $this->redirect('/');
            }


            
            //Si todo esta bien, debemos generar las preguntas al azar para este intento.
            $total = 0;
            
             $attempt->replies = [];

            //Recorremos las categorías,
            foreach($user->tests[0]->test_type->categories as $category) {
                
                //Vemos cuantes preguntas hay que hacer
                $quantity = $category->_joinData['questions_per_test'];
                $total += $quantity;
                
                $order = 'random()';
                if(Configure::read('debug')) $order = 'rand()';
                
                //Obtenemos las preguntas en forma aleatoria
                $questions = $this->Attempts->Replies->Questions->find('all', [
                    'conditions' => [
                        'category_id' => $category->id,
                        'active' => true
                    ],
                    'order' => $order,
                    'limit' => $quantity,
                ])->toArray();

                //shuffle ($q1) ;
                
                //$questions = array_slice ($q1, 0, $quantity) ;

                foreach($questions as $question) {
                    $reply = $this->Attempts->Replies->newEntity();
                    
                    $reply->question = $question;

                    $attempt->replies[] = $reply;

                }                
            }

            $attempt->test = $user->tests[0];
            
            if ($this->Attempts->save($attempt)) {

                $this->Flash->success(__('Welcome {0}. You have {1} minutes to answer the {2} questions of this test.', $user->fullname, $user->tests[0]->test_type->time, $total));

                $attempt->user = $this->Attempts->Tests->Users->get($user->id);

                $session = $this->request->session();
                $session->write('attempt', $attempt);
                $session->write('user', $user);
                $session->write('token', $token);

                return $this->redirect(['controller' => 'attempts', 'action' => 'begin']);
                
            } else {
                $this->Flash->error(__('The attempt could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('attempt', 'token'));

    }
    
    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function repeat($id) {
        
        $attempt = $this->Attempts->newEntity();
        
        if ($this->request->is('put')) {
        
            $originalAttempt = $this->request->session()->read('attempt');
            
            $user = $this->request->session()->read('user');
            
            $email = $user->email;
            $token = $this->request->session()->read('token');
            
            if(!$user->tests[0]->modified->wasWithinLast($user->tests[0]->active_for.' hours')) {
                $this->Flash->error('El tiempo para rendir el examen ha caducado. Por favor, contacte con el instructor.');
                
                return $this->redirect('/');
            }

            if(count($user->tests[0]->attempts) == $user->tests[0]->allowed_attempts ) {
                $this->Flash->error('Ya ha utilizado todos los intentos disponibles para este exámen. Por favor, contacte con el instructor.');
                
                return $this->redirect('/');
            }

            //Si todo esta bien, debemos generar las preguntas al azar para este intento.
            $total = 0;
            
            $attempt->replies = [];

            //Recorremos las categorías,
            foreach($user->tests[0]->test_type->categories as $category) {
                
                //Vemos cuantes preguntas hay que hacer
                $quantity = $category->_joinData['questions_per_test'];
                $total += $quantity;
                
                $order = 'random()';
                if(Configure::read('debug')) $order = 'rand()';
                
                //Obtenemos las preguntas en forma aleatoria
                $questions = $this->Attempts->Replies->Questions->find('all', [
                    'conditions' => [
                        'category_id' => $category->id,
                        'active' => true
                    ],
                    'order' => $order,
                    'limit' => $quantity,
                ])->toArray();

                foreach($questions as $question) {
                    $reply = $this->Attempts->Replies->newEntity();
                    
                    $reply->question = $question;

                    $attempt->replies[] = $reply;

                }                
            }

            $attempt->test = $user->tests[0];
            
            if ($this->Attempts->save($attempt)) {

                $this->Flash->success(__('You have {0} minutes to answer the {1} questions of this test.', $user->tests[0]->test_type->time, $total));

                $attempt->user = $this->Attempts->Tests->Users->get($user->id);

                $session = $this->request->session();
                $session->write('attempt', $attempt);

                return $this->redirect(['controller' => 'attempts', 'action' => 'begin']);
                
            } else {
                $this->Flash->error(__('The attempt could not be saved. Please, try again.'));
            }
        
            $this->set(compact('attempt', 'token'));
        
        } else {
            return $this->redirect(['controller' => 'attempts', 'action' => 'add']);
        }

    }
    
    public function begin() {
        if($this->request->is(['put','post'])) {
            
            $session = $this->request->session();
            $attempt = $session->read('attempt');
            
            if($this->request->data('id') != $attempt->id) {
                $this->Flash->error('Lo lamentamos, se ha producido un error. Por favor, reintente en unos instantes.');
                
                return $this->redirect('/');
            }
            //$attempt = $this->Attempts->get($this->request->data('id'));
            $attempt->started_at = Time::now();
            
            if ($this->Attempts->save($attempt)) {
                
                $session->write('attempt', $attempt);
                $session->write('reply_number', 0);
                
                return $this->redirect(['controller' => 'replies', 'action' => 'add']);
            } else {
                $this->Flash->error(__('Algo salió mal. accion:BEGIN controller:Attempts'));
                 
                return $this->redirect('/');
            }
            
        }
        
        $attempt = $this->request->session()->read('attempt');
        
        $this->set(compact('attempt'));
        
    }
    
    public function complete() {
        $session = $this->request->session();
        $attempt = $session->read('attempt');
        
        $total = count($attempt->replies);
        $correct = 0;
        
        foreach($attempt->replies as $reply) {
            $answer = $this->Attempts->Replies->Answers->get($reply->answer_id);
            if($answer->correct) $correct++;
        }
        
        $correctPercent = $correct / $total * 100;
        
        $required = $attempt->test->test_type->required;
            
               
        
        if ($correctPercent >= $required) {
            
            $msg = "Felicitaciones ".h($attempt->user->fullname)."\nUsted ha respondido correctamente el ".sprintf("%.2f%%", $correctPercent)." de las preguntas, por lo cual ha aprobado este examen.";
            
            $this->Flash->success($msg);
        } else {
            
            $msg = "Usted ha respondido correctamente el ".sprintf("%.2f%%", $correctPercent)." de las preguntas, lo cual no alcanza para aprobar este examen.";
            
            $this->Flash->warning($msg);
        }
        
         $quedan = $attempt->test->allowed_attempts - count($attempt->test->attempts) - 1;
        
        
        $this->set(compact('attempt', 'quedan'));

    }
}
