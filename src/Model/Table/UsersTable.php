<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('fullname');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        
        $this->hasMany('tests', [
            'foreignKey' => 'user_id'
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->email('email', false, __('Please, insert a valid email address.'))
            ->requirePresence('email', 'create')
            ->notEmpty('email', __('You must enter a email address.'));

        $validator
            ->requirePresence('firstname', 'create')
            ->notEmpty('firstname', __('You must enter your first name.'));

        $validator
            ->allowEmpty('lastname');

        $validator
            ->minLength('password', 6, __('The password must have at least 6 characters.'))
            ->allowEmpty('password');

        $validator
            ->sameAs('password_confirmation', 'password', __('Both passwords must match.'));

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        $validator
            ->requirePresence('role', 'create')
            ->inList('role', ['admin', 'user'], __('Please, enter a valid role.'))
            ->notEmpty('role');

        $validator
            ->allowEmpty('reset_token');

        $validator
            ->dateTime('reseted_at')
            ->allowEmpty('reseted_at');

        $validator
            ->allowEmpty('remember_token');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
    
    public function createToken(){
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $vChars = str_split($chars.$chars);
        shuffle($vChars);
        return implode(array_slice($vChars,0,32));
    }
    
    
}
