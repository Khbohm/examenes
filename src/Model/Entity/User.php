<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $email
 * @property string $firstname
 * @property string $lastname
 * @property string $password
 * @property bool $active
 * @property string $role
 * @property string $reset_token
 * @property \Cake\I18n\Time $reseted_at
 * @property string $remember_token
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
    
    protected function _setPassword($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }    
    
    protected function _setResetToken($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }    

    protected function _setRememberToken($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    } 
    
    protected function _getFullname()
    {
        return trim($this->_properties['firstname'] . '  ' .
            $this->_properties['lastname']);
    }
    
}
