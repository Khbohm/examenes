<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CategoriesTestTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CategoriesTestTypesTable Test Case
 */
class CategoriesTestTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CategoriesTestTypesTable
     */
    public $CategoriesTestTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.categories_test_types',
        'app.categories',
        'app.questions',
        'app.answers',
        'app.test_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CategoriesTestTypes') ? [] : ['className' => 'App\Model\Table\CategoriesTestTypesTable'];
        $this->CategoriesTestTypes = TableRegistry::get('CategoriesTestTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CategoriesTestTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
