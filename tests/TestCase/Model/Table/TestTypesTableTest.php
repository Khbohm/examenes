<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TestTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TestTypesTable Test Case
 */
class TestTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TestTypesTable
     */
    public $TestTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.test_types',
        'app.categories',
        'app.questions',
        'app.answers',
        'app.categories_test_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TestTypes') ? [] : ['className' => 'App\Model\Table\TestTypesTable'];
        $this->TestTypes = TableRegistry::get('TestTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TestTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
