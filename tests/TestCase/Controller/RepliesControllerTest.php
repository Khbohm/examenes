<?php
namespace App\Test\TestCase\Controller;

use App\Controller\RepliesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\RepliesController Test Case
 */
class RepliesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.replies',
        'app.attempts',
        'app.tests',
        'app.users',
        'app.tests',
        'app.categories',
        'app.questions',
        'app.answers'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
