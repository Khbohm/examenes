<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\TestTypesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\TestTypesController Test Case
 */
class TestTypesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.test_types',
        'app.categories',
        'app.questions',
        'app.answers',
        'app.categories_test_types'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
