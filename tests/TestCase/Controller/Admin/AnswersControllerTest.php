<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\AnswersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\AnswersController Test Case
 */
class AnswersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.answers',
        'app.questions'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
