<?php
namespace App\Test\TestCase\Controller;

use App\Controller\AttemptsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\AttemptsController Test Case
 */
class AttemptsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.attempts',
        'app.tests',
        'app.users',
        'app.tests',
        'app.categories',
        'app.questions',
        'app.answers',
        'app.replies'
    ];

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
