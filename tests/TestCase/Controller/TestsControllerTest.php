<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TestsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TestsController Test Case
 */
class TestsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tests',
        'app.users',
        'app.tests',
        'app.categories',
        'app.questions',
        'app.answers',
        'app.attempts',
        'app.replies'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
